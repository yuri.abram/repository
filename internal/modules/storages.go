package modules

import (
	"gitlab/repository/internal/db/adapter"
	"gitlab/repository/internal/modules/user/storage"
)

type Storages struct {
	Users *storage.UserStorage
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Users: storage.NewUserStorage(sqlAdapter),
	}
}
