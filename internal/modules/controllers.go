package modules

import (
	"gitlab/repository/internal/infrastructure/component"
	ucontroller "gitlab/repository/internal/modules/user/controller"
)

type Controllers struct {
	User *ucontroller.User
}

func NewControllers(services *Services, components *component.Components) *Controllers {

	return &Controllers{
		User: ucontroller.NewUserCtl(services.UserService, components),
	}
}
