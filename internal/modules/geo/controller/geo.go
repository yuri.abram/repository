package controller

import (
	"encoding/json"
	"fmt"
	"gitlab/repository/internal/infrastructure/component"
	"gitlab/repository/internal/infrastructure/responder"
	"gitlab/repository/internal/modules/geo/service"
	"net/http"
)

// Geo
// responder - интерфейс из бойрплейта для http ответов.
// service - интерфейс для запроса данных в DaData
type Geo struct {
	responder responder.Responder
	service   service.GeoServicer
}

func NewGeo(service service.GeoServicer, components *component.Components) *Geo {
	return &Geo{responder: components.Responder, service: service}
}

type GeoResponder interface {
	GeoCoding(w http.ResponseWriter, r *http.Request)
	SearchResponding(w http.ResponseWriter, r *http.Request)
}

// GeoCoding
// @summary 		Получить адрес по координатам
// @Description 	Получить адрес по координатам
// @Tags 			geocode
// @Accept 			json
// @Produce 		json
// @Security 		Bearer
//
// param 	param name, param type, data type, is mandatory?, comment attribute(optional)
//
// @Param			request body 	service.GeocodeRequest true "Geocode request"
// @Success 		200 {object} 	service.GeocodeResponse
// @Failure 		400	{string}	string "Bad request"
// @Failure 		401	{string}	string "Unauthorized"
// @Failure 		500	{string}	string "Internal server error"
// @Router 			/api/address/geocode [post]
func (g *Geo) GeoCoding(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		g.responder.ErrorBadRequest(w, fmt.Errorf("bad method"))
		return
	}

	var gr service.GeocodeRequest
	err := json.NewDecoder(r.Body).Decode(&gr)
	if err != nil {
		g.responder.ErrorBadRequest(w, err)
		return
	}
	// geo/service/geocode.go
	out, err := g.service.Geocode(r.Context(), &gr)
	if err != nil {
		g.responder.ErrorBadRequest(w, err)
	}
	g.responder.OutputJSON(w, out)
}

// SearchResponding
// @Summary 		Поиск адреса
// @Description Поиск адреса./
// @ID 			search
// @Tags 		geocode
// @Accept 		json
// @Produce 	json
// @Security 	Bearer
// @Param 		request body 		service.SearchRequest true "Search request"
// @Success 	200 	{object} 	service.SearchResponse
// @Failure 	400		{string}	string "Bad request"
// @Failure 	401		{string}	string "Unauthorized"
// @Failure 	500		{string}	string "Internal server error"
// @Router		/api/address/search [post]
func (g *Geo) SearchResponding(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		g.responder.ErrorBadRequest(w, fmt.Errorf("bad method"))
		return
	}

	var sr service.SearchRequest

	err := json.NewDecoder(r.Body).Decode(&sr)
	if err != nil {
		g.responder.ErrorBadRequest(w, err)
		return
	}

	out, err := g.service.Search(r.Context(), &sr)
	if err != nil {
		g.responder.ErrorBadRequest(w, err)
	}

	g.responder.OutputJSON(w, out)

}
