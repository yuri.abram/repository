package service

import (
	"context"
	"encoding/json"
)

type SearchRequest struct {
	Query string `json:"query"`
}

type SearchResponse struct {
	Addresses []*Address `json:"suggestions"`
}

// Search
// @Summary 		Поиск адреса
// @Description Поиск адреса./
// @ID 			search
// @Tags 		geocode
// @Accept 		json
// @Produce 	json
// @Security 	Bearer
// @Param 		request body 		service.SearchRequest true "Search request"
// @Success 	200 	{object} 	service.SearchResponse
// @Failure 	400		{string}	string "Bad request"
// @Failure 	401		{string}	string "Unauthorized"
// @Failure 	500		{string}	string "Internal server error"
// @Router		/api/address/search [post]
func (g *GeoService) Search(ctx context.Context, rq *SearchRequest) (SearchResponse, error) {

	resp, err := doRequest(rq)
	if err != nil {
		return SearchResponse{}, err
	}

	var searchResponse SearchResponse

	err = json.NewDecoder(resp.Body).Decode(&searchResponse)
	if err != nil {
		return SearchResponse{}, err
	}
	return searchResponse, nil
}
