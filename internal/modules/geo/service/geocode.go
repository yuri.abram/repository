package service

import (
	"context"
	"encoding/json"
)

type GeocodeRequest struct {
	Lat string `json:"lat" example:"55.753994" validate:"required"`
	Lng string `json:"lon" example:"37.620003" validate:"required"`
}

type GeocodeResponse struct {
	Addresses []*Address `json:"suggestions"`
}

// Geocode
// @summary 		Получить адрес по координатам
// @Description 	Получить адрес по координатам
// @Tags 			geocode
// @Accept 			json
// @Produce 		json
// @Security 		Bearer
//
// param 	param name, param type, data type, is mandatory?, comment attribute(optional)
//
// @Param			request body 	service.GeocodeRequest true "Geocode request"
// @Success 		200 {object} 	service.GeocodeResponse
// @Failure 		400	{string}	string "Bad request"
// @Failure 		401	{string}	string "Unauthorized"
// @Failure 		500	{string}	string "Internal server error"
// @Router 			/api/address/geocode [post]
func (g *GeoService) Geocode(ctx context.Context, rq *GeocodeRequest) (GeocodeResponse, error) {

	// doRequest() from apiclient.go
	resp, err := doRequest(rq)
	if err != nil {
		return GeocodeResponse{}, err
	}

	var geocodeResponse GeocodeResponse
	err = json.NewDecoder(resp.Body).Decode(&geocodeResponse)
	if err != nil {
		return GeocodeResponse{}, err
	}

	return geocodeResponse, nil

}
