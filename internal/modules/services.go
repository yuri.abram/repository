package modules

import (
	"gitlab/repository/internal/infrastructure/component"
	userv "gitlab/repository/internal/modules/user/service"
)

type Services struct {
	UserService *userv.UserService
}

func NewServices(storage *Storages, components *component.Components) *Services {
	return &Services{
		UserService: userv.NewUserService(storage.Users, components.Logger),
	}
}
