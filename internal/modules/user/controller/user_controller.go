package controller

import (
	"encoding/json"
	"fmt"
	"gitlab/repository/internal/db/adapter"
	"gitlab/repository/internal/infrastructure/component"
	"gitlab/repository/internal/infrastructure/responder"
	"gitlab/repository/internal/modules/user/service"
	"net/http"
)

type UserController interface {
	CreateUser(w http.ResponseWriter, r *http.Request)
	UpdateUser(w http.ResponseWriter, r *http.Request)
	DeleteUser(w http.ResponseWriter, r *http.Request)
	ListUser(w http.ResponseWriter, r *http.Request)
	GetByID(w http.ResponseWriter, r *http.Request)
}

type User struct {
	service   service.UserServicer
	responder responder.Responder
}

func NewUserCtl(service service.UserServicer, components *component.Components) *User {
	return &User{
		service:   service,
		responder: components.Responder,
	}
}

// CreateUser
// @Summary Create new user
// @Tags User
// @Accept json
// @Produce json
// @Param request body BasicRequest true "Create user request"
// @Success 200 {object} BasicResponse
// @Failure 400 {object} BasicResponse
// @Failure 500 {object} BasicResponse
// @Router /api/create [post]
func (u *User) CreateUser(w http.ResponseWriter, r *http.Request) {
	var rq BasicRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}
	err = u.service.Create(r.Context(), rq.User)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, BasicResponse{
		Success: true,
		Message: "user created",
	})
}

// UpdateUser
// @Summary Update user
// @Tags User
// @Accept json
// @Produce json
// @Param request body BasicRequest true "Update user request"
// @Success 200 {object} BasicResponse
// @Failure 400 {object} BasicResponse
// @Failure 500 {object} BasicResponse
// @Router /api/update [post]
func (u *User) UpdateUser(w http.ResponseWriter, r *http.Request) {
	var rq BasicRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}
	err = u.service.Update(r.Context(), rq.User)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, BasicResponse{
		Success: true,
		Message: "user was updated",
	})

}

// DeleteUser
// @Summary Delete user
// @Tags User
// @Accept json
// @Produce json
// @Param request body IDRequest true "Delete user request"
// @Success 200 {object} BasicResponse
// @Failure 400 {object} BasicResponse
// @Failure 500 {object} BasicResponse
// @Router /api/delete [post]
func (u *User) DeleteUser(w http.ResponseWriter, r *http.Request) {
	var rq IDRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}
	err = u.service.Delete(r.Context(), rq.UserID)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, BasicResponse{
		Success: true,
		Message: "user is now marked deleted",
	})
}

// ListUser
// @Summary List users
// @Tags User
// @Accept json
// @Produce json
// @Param request body ListRequest true "List user request"
// @Success 200 {object} ListResponse
// @Failure 400 {object} BasicResponse
// @Failure 500 {object} BasicResponse
// @Router /api/list [post]
func (u *User) ListUser(w http.ResponseWriter, r *http.Request) {
	var rq ListRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}
	rqCondition := adapter.Condition{
		LimitOffset: &adapter.LimitOffset{
			Limit:  rq.Limit,
			Offset: rq.Offset,
		},
	}
	users, count, err := u.service.List(r.Context(), rqCondition)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, ListResponse{
		Success: true,
		Message: fmt.Sprintf("%d user(s) listed ", count),
		Data:    users,
	})
}

// GetByID
// @Summary Get user
// @Tags User
// @Produce json
// @Param request body IDRequest true "Get user request"
// @Success 200 {object} GetByIDResponse
// @Failure 400 {object} BasicResponse
// @Failure 500 {object} BasicResponse
// @Router /api/users/{id} [get]
func (u *User) GetByID(w http.ResponseWriter, r *http.Request) {
	var rq IDRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}
	user, err := u.service.GetByID(r.Context(), rq.UserID)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, GetByIDResponse{
		Success: true,
		Message: "user has been found",
		Data:    user,
	})
}
