package controller

import "gitlab/repository/internal/models"

type RegisterRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	RetypePassword string `json:"retype_password"`
	IdempotencyKey string `json:"idempotency_key"`
}

type Data struct {
	User models.UserDTO `json:"user,omitempty"`
}

type BasicRequest struct {
	User models.UserDTO `json:"user,omitempty"`
}

type IDRequest struct {
	UserID string `json:"id"`
}

type ListRequest struct {
	Limit  int64 `json:"limit"`
	Offset int64 `json:"offset"`
}

type ListResponse struct {
	Success bool             `json:"success"`
	Message string           `json:"message"`
	Data    []models.UserDTO `json:"data"`
}

type GetByIDResponse struct {
	Success bool           `json:"success"`
	Message string         `json:"message"`
	Data    models.UserDTO `json:"data"`
}

type BasicResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}
