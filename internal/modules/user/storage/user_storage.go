package storage

import (
	"context"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"gitlab/repository/internal/db/adapter"
	"gitlab/repository/internal/infrastructure/db/scanner"
	"gitlab/repository/internal/models"
	"time"
)

type UserRepository interface {
	Create(ctx context.Context, u models.UserDTO) error
	Update(ctx context.Context, u models.UserDTO) error
	GetByID(ctx context.Context, userID int) (models.UserDTO, error)
	List(ctx context.Context, condition adapter.Condition) ([]models.UserDTO, error)
	Delete(ctx context.Context, userID int) error
}

type UserStorage struct {
	adapter *adapter.SQLAdapter
}

func NewUserStorage(sqlAdapter *adapter.SQLAdapter) *UserStorage {
	return &UserStorage{adapter: sqlAdapter}
}

func (us *UserStorage) Create(ctx context.Context, u models.UserDTO) error {
	return us.adapter.Create(ctx, &u)
}

// Update - обновление пользователя в БД
func (us *UserStorage) Update(ctx context.Context, u models.UserDTO) error {
	err := us.adapter.Update(ctx, &u, adapter.Condition{
		Equal: sq.Eq{
			"id": u.GetID(),
		},
	}, scanner.Update)
	if err != nil {
		return err
	}
	return nil
}

func (us *UserStorage) GetByID(ctx context.Context, userID int) (models.UserDTO, error) {
	var dto models.UserDTO
	var err error

	// если данных нет в кеше, то получаем их из БД
	var list []models.UserDTO
	err = us.adapter.List(ctx, &list, dto.TableName(), adapter.Condition{
		Equal: map[string]interface{}{
			"id": userID,
		},
	})
	if err != nil {
		return models.UserDTO{}, err
	}
	if len(list) < 1 {
		return models.UserDTO{}, fmt.Errorf("user storage: GetByID not found")
	}
	return list[0], nil
}

func (us *UserStorage) Delete(ctx context.Context, userID int) error {
	user, err := us.GetByID(ctx, userID)
	if err != nil {
		return err
	}
	if user.DeletedAt.Valid {
		return fmt.Errorf("user already deleted")
	}
	user.SetDeletedAt(time.Now())
	return us.Update(ctx, user)
}

func (us *UserStorage) List(ctx context.Context, condition adapter.Condition) ([]models.UserDTO, error) {
	var users []models.UserDTO
	err := us.adapter.List(ctx, &users, "users", condition)
	if err != nil {
		return nil, err
	}
	return users, nil
}
