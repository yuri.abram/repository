package service

import (
	"context"
	"gitlab/repository/internal/db/adapter"
	"gitlab/repository/internal/models"
	"gitlab/repository/internal/modules/user/storage"
	"go.uber.org/zap"
	"strconv"
)

type UserServicer interface {
	Create(ctx context.Context, u models.UserDTO) error
	Update(ctx context.Context, u models.UserDTO) error
	List(ctx context.Context, condition adapter.Condition) ([]models.UserDTO, int, error)
	Delete(ctx context.Context, userID string) error
	GetByID(ctx context.Context, userID string) (models.UserDTO, error)
}

type UserService struct {
	storage storage.UserRepository
	logger  *zap.Logger
}

func NewUserService(storage storage.UserRepository, logger *zap.Logger) *UserService {
	return &UserService{
		storage: storage,
		logger:  logger,
	}
}

func (us *UserService) Create(ctx context.Context, u models.UserDTO) error {
	err := us.storage.Create(ctx, u)
	if err != nil {
		us.logger.Error("failed to create user", zap.Error(err))
		return err
	}
	return nil
}

func (us *UserService) Update(ctx context.Context, u models.UserDTO) error {
	err := us.storage.Update(ctx, u)
	if err != nil {
		us.logger.Error("failed to update user", zap.Error(err))
		return err
	}
	return nil
}

func (us *UserService) List(ctx context.Context, condition adapter.Condition) ([]models.UserDTO, int, error) {
	users, err := us.storage.List(ctx, condition)
	if err != nil {
		us.logger.Error("failed to get users", zap.Error(err))
		return nil, 0, err
	}
	return users, len(users), nil
}

func (us *UserService) Delete(ctx context.Context, userID string) error {
	userIDInt, err := strconv.Atoi(userID)
	if err != nil {
		us.logger.Error("failed to convert userID", zap.Error(err))
		return err
	}
	err = us.storage.Delete(ctx, userIDInt)
	if err != nil {
		us.logger.Error("failed to delete user", zap.Error(err))
		return err
	}
	return nil
}

func (us *UserService) GetByID(ctx context.Context, userID string) (models.UserDTO, error) {
	userIDInt, err := strconv.Atoi(userID)
	if err != nil {
		us.logger.Error("failed to convert userID", zap.Error(err))
		return models.UserDTO{}, err
	}
	user, err := us.storage.GetByID(ctx, userIDInt)
	if err != nil {
		us.logger.Error("failed to get user", zap.Error(err))
		return models.UserDTO{}, err
	}
	return user, nil
}
