package service

import (
	"context"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

// Register - register new user
// @summary 		Регистрация нового пользователя
// @description 	Регистрация нового пользователя.
// @Tags 			auth
// @Accept 			json
// @Produce 		json
// @Param 			request body service.UserReq true "User request"
// @success 200 {string} string "User registered"
// @Failure 400 {string} string "Bad request"
// @Router /api/register [post]
func (db *UsersDB) Register(ctx context.Context, user UserReq) error {

	// проверка что пользователь существует
	if _, ok := db.users[user.Username]; ok {
		return fmt.Errorf("user already exists")
	}

	hash, _ := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)

	db.users[user.Username] = string(hash)

	return nil
}
