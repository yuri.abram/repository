package controller

import (
	"encoding/json"
	"github.com/ptflp/godecoder"
	"gitlab/repository/internal/infrastructure/component"
	"gitlab/repository/internal/infrastructure/responder"
	"gitlab/repository/internal/modules/auth/service"
	"net/http"
)

type Auther interface {
	Register(http.ResponseWriter, *http.Request)
	Login(http.ResponseWriter, *http.Request)
}

type Auth struct {
	auth service.Auther
	responder.Responder
	godecoder.Decoder
}

// Register - register new user
// @summary 		Регистрация нового пользователя
// @description 	Регистрация нового пользователя.
// @Tags 			auth
// @Accept 			json
// @Produce 		json
// @Param 			request body service.UserReq true "User request"
// @success 200 {string} string "User registered"
// @Failure 400 {string} string "Bad request"
// @Router /api/register [post]
func (a *Auth) Register(w http.ResponseWriter, r *http.Request) {
	userReq := service.UserReq{}

	err := json.NewDecoder(r.Body).Decode(&userReq)
	if err != nil {
		http.Error(w, "Error decoding request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	err = a.auth.Register(r.Context(), userReq)
	if err != nil {
		http.Error(w, "Error registering user: "+err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("User registered"))
}

// Login - login user
// @summary 		Логин пользователя
// @description 	Логин пользователя.
// @Tags 			auth
// @Accept 			json
// @Produce 		json
// @Param 			request body service.UserReq true "User request"
// @success 200 {string} string "User logged in"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Invalid credentials"
// @Router /api/login [post]
func (a *Auth) Login(w http.ResponseWriter, r *http.Request) {
	userReq := service.UserReq{}
	err := json.NewDecoder(r.Body).Decode(&userReq)
	if err != nil {
		http.Error(w, "Error decoding request body: "+err.Error(), http.StatusBadRequest)
	}

	tokenString, err := a.auth.Login(r.Context(), userReq)
	if err != nil {
		http.Error(w, "Error logging in: "+err.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(tokenString))
}

func NewAuth(service service.Auther, components *component.Components) Auther {
	return &Auth{auth: service, Responder: components.Responder, Decoder: components.Decoder}
}
