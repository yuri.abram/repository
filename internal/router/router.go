package router

import (
	"github.com/go-chi/chi"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab/repository/docs"
	"gitlab/repository/internal/infrastructure/component"
	"gitlab/repository/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()
	user := controllers.User

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	r.Group(func(r chi.Router) {
		r.Route("/api", func(r chi.Router) {
			r.Get("/swagger/*", httpSwagger.Handler(
				httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
			))
			r.Get("/users/{id}", user.GetByID)
			r.Post("/create", user.CreateUser)
			r.Post("/update", user.UpdateUser)
			r.Post("/list", user.ListUser)
			r.Post("/delete", user.DeleteUser)

		})
	})

	return r
}
