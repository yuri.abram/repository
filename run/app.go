package run

import (
	"context"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab/repository/config"
	"gitlab/repository/internal/db"
	"gitlab/repository/internal/infrastructure/component"
	"gitlab/repository/internal/infrastructure/db/migrate"
	"gitlab/repository/internal/infrastructure/db/scanner"
	"gitlab/repository/internal/infrastructure/errors"
	"gitlab/repository/internal/infrastructure/responder"
	"gitlab/repository/internal/infrastructure/router"
	"gitlab/repository/internal/infrastructure/server"
	"gitlab/repository/internal/models"
	"gitlab/repository/internal/modules"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"net/http"
	"os"
	"time"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstrapper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstrapper - интерфейс инициализации приложения
type Bootstrapper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Serverer
	Sig      chan os.Signal
	Storages *modules.Storages
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})

	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)

	// инициализация компонентов
	components := component.NewComponents(a.conf, responseManager, a.logger, decoder)

	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.UserDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := modules.NewStorages(sqlAdapter)
	a.Storages = newStorages

	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	a.Servises = services
	controllers := modules.NewControllers(services, components)

	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:         ":8080",
		Handler:      r,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a

}
